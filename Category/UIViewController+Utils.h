//
//  UIViewController+Utils.h
//  UVCocoaLibrary
//
//  Created by chenjiaxin on 10/18/13.
//  Copyright (c) 2013 XXXX.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Utils)

/**
 * 在视图上增加事件 点击视图自动隐藏键盘
 */
-(void)addTapHideKeyBroard;
@end
